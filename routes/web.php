<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
/*
Route::get('/index', function () {
    return view('halaman.index');
});

Route::get('/book', function () {
    return view('halaman.book');
});

Route::get('/menu', function () {
    return view('halaman.menu');
});

Route::get('/about', function () {
    return view('halaman.about');
});

Route::get('/book', 'OrderController@order');
*/

/* Route untuk Main Page */
Route::get('/', 'IndexController@index');
Route::get('/book', 'OrderController@order');
Route::get('/menu', 'MenuController@listmenu');
Route::get('/about', 'AboutController@ours');
Route::get('/orderonline', 'OnlineController@online');

/* Route untuk CRUD tabel kategori */
Route::resource('kategori', 'KategoriController');








Route::get('/testingcommitmaintainer', function (){
    return view('testing');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
